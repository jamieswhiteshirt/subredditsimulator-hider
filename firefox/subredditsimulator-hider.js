(function() {
	var getLinkAuthor = function getLinkAuthor(linkElement) {
		var authorElement = linkElement.querySelector(".entry > .top-matter > .tagline > .author");
		return authorElement.text;
	};

	var replaceDomain = function replaceDomain(linkElement, subreddit) {
		var domainElement = linkElement.querySelector(".entry > .top-matter > .title > .domain > a");

		if (domainElement) {
			domainElement.text = domainElement.text.replace("SubredditSimulator", subreddit);
		}
	};

	var replaceAuthor = function replaceAuthor(linkElement, usernames) {
		var authorElement = linkElement.querySelector(".entry > .top-matter > .tagline > .author");
		authorElement.text = usernames[Math.floor(Math.random() * usernames.length)];
	};

	var replaceSubreddit = function replaceSubreddit(linkElement, subreddit) {
		var subredditElement = linkElement.querySelector(".entry > .top-matter > .tagline > .subreddit");

		if (subredditElement) {
			subredditElement.text = "r/" + subreddit;
		}
	};

	var visitSSLinkElement = function visitSSLinkElement(linkElement, usernames) {
		if (!linkElement.SSreplaced) {
			var subreddit_SS = linkElement.getAttribute("data-author");
			var subreddit = subreddit_SS.substring(0, subreddit_SS.length - 3);

			replaceDomain(linkElement, subreddit);
			replaceAuthor(linkElement, usernames);
			replaceSubreddit(linkElement, subreddit);

			linkElement.SSreplaced = true;
		}
	};

	var visitAllSSLinkElements = function visitAllSSLinkElements() {
		var allLinkElements = document.querySelectorAll("#siteTable > .link");

		var allSSLinkElements = [];
		var allOtherLinkElements = [];

		for (var i = 0; i < allLinkElements.length; i++) {
			var linkElement = allLinkElements[i];
			if (linkElement.getAttribute("data-subreddit") === "SubredditSimulator") {
				allSSLinkElements.push(linkElement);
			}
			else {
				allOtherLinkElements.push(linkElement);
			}
		}

		var usernames = [];

		for (var i = 0; i < allOtherLinkElements.length; i++) {
			usernames.push(getLinkAuthor(allOtherLinkElements[i]));
		}

		for (var i = 0; i < allSSLinkElements.length; i++) {
			visitSSLinkElement(allSSLinkElements[i], usernames);
		}
	};

	visitAllSSLinkElements();

	MutationObserver = window.WebKitMutationObserver;

	var observer = new MutationObserver(function(mutations, observer) {
		visitAllSSLinkElements();
	});

	observer.observe(document, {
		subtree: true,
		attributes: true
	});
})();
